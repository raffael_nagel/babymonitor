package api.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Raffael on 25/02/2017.
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private String tokenHeader = "X-Access-Token";

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        String xAuth = httpServletRequest.getHeader(tokenHeader);

        if(xAuth == null){
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized: No Access Token");
            return;
        }
        if(!xAuth.equals("teste")){
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized: Invalid Access Token");
            return;
        }

        Authentication auth = new UsernamePasswordAuthenticationToken("teste", null, null);
        SecurityContextHolder.getContext().setAuthentication(auth);

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
