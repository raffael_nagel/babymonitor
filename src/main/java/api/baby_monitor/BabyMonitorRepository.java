package api.baby_monitor;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Raffael on 04/03/2017
 */
public interface BabyMonitorRepository extends MongoRepository<BabyMonitor, String>{

    public List<BabyMonitor> findByAction(String action);

}
