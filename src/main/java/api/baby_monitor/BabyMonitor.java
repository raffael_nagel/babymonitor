package api.baby_monitor;

import org.springframework.data.annotation.Id;

/**
 * Created by Raffael on 04/03/2017
 */
public class BabyMonitor {

    @Id
    private final String id;
    private final String action;
    private final String start_timestamp;
    private final String end_timestamp;

    public BabyMonitor(String id, String action, String sTime, String eTime){
        this.id = id;
        this.action = action;
        this.start_timestamp = sTime;
        this.end_timestamp = eTime;
    }

    public String getId(){
        return this.id;
    }
    public String getAction(){
        return this.action;
    }
    public String getStart(){
        return this.start_timestamp;
    }
    public String getEnd(){
        return this.end_timestamp;
    }

    @Override
    public String toString() {
        return String.format(
                "Event[id=%d, action='%s', start='%s', end=%s]",
                id, action, start_timestamp, end_timestamp);
    }

}
