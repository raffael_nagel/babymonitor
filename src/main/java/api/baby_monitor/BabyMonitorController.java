package api.baby_monitor;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Baby Monitor
 */
@RestController
@RequestMapping("/api/baby_monitor/logs")
public class BabyMonitorController {

    @RequestMapping(method = RequestMethod.GET)
    public BabyMonitor[] listLogs(){

        BabyMonitor[] events = new BabyMonitor[5];
        events[0] = new BabyMonitor("0","comeu","2015-04-04 11:00:00.000","2015-04-04 12:00:00.000");
        events[1] = new BabyMonitor("1","dormiu","2015-04-04 11:00:00.000","2015-04-04 12:00:00.000");
        events[2] = new BabyMonitor("2","chorou","2015-04-04 11:00:00.000","2015-04-04 12:00:00.000");
        events[3] = new BabyMonitor("3","comeu","2015-04-04 11:00:00.000","2015-04-04 12:00:00.000");
        events[4] = new BabyMonitor("4","dormiu","2015-04-04 11:00:00.000","2015-04-04 12:00:00.000");
        return events;
    }

    @RequestMapping(value = "/{logId}", method = RequestMethod.GET)
    public BabyMonitor getLog(@PathVariable String logId){
        return new BabyMonitor(logId,"dormiu","2015-04-04 11:00:00.000","2015-04-04 12:00:00.000");
    }


}
