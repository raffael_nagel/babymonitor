package api;

import api.baby_monitor.BabyMonitor;
import api.baby_monitor.BabyMonitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Raffael on 25/02/2017.
 */
@SpringBootApplication
public class Application implements CommandLineRunner{

    @Autowired
    private BabyMonitorRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

        // save a couple of customers
        repository.save(new BabyMonitor("0","comeu","2015-04-04 11:00:00.000","2015-04-04 12:00:00.000"));
        repository.save(new BabyMonitor("0","dormiu","2015-04-04 11:00:00.000","2015-04-04 12:00:00.000"));

        // fetch all customers
        System.out.println("Logs found with findAll():");
        System.out.println("-------------------------------");
        for (BabyMonitor log : repository.findAll()) {
            System.out.println(log);
        }
        System.out.println();

    }
}
